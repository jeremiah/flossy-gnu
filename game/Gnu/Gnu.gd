extends KinematicBody2D

signal died

onready var tween = $Tween
onready var anim_player = $AnimationPlayer
onready var sprite = $Sprite
onready var collision_shape = $CollisionShape2D

export var gravity = 4000.0
export var flap_force = 1200.0
export var max_fall_speed = 1200.0
export var max_angular_velocity = PI
export var angular_acceleration = 2.0 * PI
export var speed_horizontal = 280.0

var _velocity = Vector2(speed_horizontal, 0.0)
var _angular_velocity = 0.0
var _idle = true setget set_idle
var _target_angle = 0.0

var _active = true setget _set_active


func _ready():
	self._idle = true
	tween.connect("tween_completed", self, "_on_Tween_tween_completed")


func set_idle(value):
	_idle = value
	set_physics_process(not value)
	if not _idle:
		anim_player.play("idle")


func _unhandled_input(event):
	if event.is_action_pressed("flap"):
		if _idle:
			self._idle = false
		flap()


func flap():
	_angular_velocity = 0.0
	_velocity.y = -flap_force
	_target_angle = -PI / 5.0
	if not tween.is_active():
		tween.interpolate_property(
			sprite, 'rotation',
			sprite.rotation, _target_angle, 0.12,
			Tween.TRANS_CUBIC, Tween.EASE_OUT)
		tween.start()


func _physics_process(delta):
	_velocity.y += gravity * delta
	_velocity.y = min(_velocity.y, max_fall_speed)

	if not tween.is_active() and sprite.rotation < PI / 5.0:
		_angular_velocity += angular_acceleration * delta
		_angular_velocity = min(_angular_velocity, max_angular_velocity)
		sprite.rotate(_angular_velocity * delta)

	var motion = _velocity * delta
	# Prevent the character from moving too high up
	if position.y < -100.0 and motion.y < 0.0:
		motion.y = 0.0
	var collision = move_and_collide(motion)
	if collision:
		die()


func die():
	set_physics_process(false)
	set_process_unhandled_input(false)
	tween.interpolate_property(
		sprite, 'scale',
		sprite.scale, Vector2(), 0.4,
		Tween.TRANS_QUAD, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_completed")
	self._active = false
	emit_signal('died')


func _set_active(value):
	_active = value
	visible = value
	collision_shape.disabled = not value


func _on_Tween_tween_completed(object, key):
	tween.stop_all()
