extends Control

onready var rank_label = $Rank
onready var score_label = $Score
onready var initials_label = $Initials
onready var animation_player = $AnimationPlayer

const COLOR_TRANSPARENT = Color('00ffffff')
const COLOR_OPAQUE = Color('00ffffff')


func setup(index, score={}):
	rank_label.set_rank(index)
	score_label.text = "%03d" % score.score
	initials_label.text = score.initials


func start():
	animation_player.play("fade_in")


func _ready():
	modulate = COLOR_TRANSPARENT
