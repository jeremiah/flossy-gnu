tool
extends Node2D


export var min_extents = 100.0 setget set_min_extents
export var max_extents = 400.0 setget set_max_extents


func set_max_extents(value):
	max_extents = value
	if max_extents < min_extents:
		self.min_extents = max_extents
	else:
		update()


func set_min_extents(value):
	min_extents = min(value, max_extents)
	update()


func get_random_vertical_position(ratio=1.0):
	assert ratio >= 0.0 and ratio <= 1.0
	var extents = lerp(min_extents, max_extents, ratio) / 2.0
	var rand_position = rand_range(-extents, extents)
	return position.y + rand_position


func _draw():
	if not Engine.editor_hint:
		return
	var window_width = ProjectSettings.get('display/window/size/width')

	var rectangle_min = Rect2(
		Vector2(0.0, -min_extents / 2.0),
		Vector2(window_width, min_extents))
	var rectangle_max = Rect2(
		Vector2(0.0, -max_extents / 2.0),
		Vector2(window_width, max_extents))

	draw_rect(rectangle_min, Color("888"), false)
	draw_rect(rectangle_max, Color("ccc"), false)
