extends Node2D
"""
Spawns instances of the Pillar in a vertical area, outside the game's view
Places the Pillars based on their pivot

TODOincrease the difficulty, we can slowly expand the spawn area,
and increase the Gnu's horizontal move speed
"""

signal pillar_checkpoint_reached

onready var spawn_area = $SpawnArea

export var spawn_interval = 400.0

var pillar_scene = preload("res://Pillar/Pillar.tscn")
var WINDOW_WIDTH = ProjectSettings.get('display/window/size/width')


func _ready():
	randomize()
	for i in range(3):
		spawn_pillar()


func spawn_pillar():
	var pillar = pillar_scene.instance()
	pillar.global_position = get_random_position()
	pillar.connect("tree_exited", self, "spawn_pillar")
	pillar.connect("checkpoint_reached", self, "emit_signal", ["pillar_checkpoint_reached"])
	add_child(pillar)
	position.x += spawn_interval


func get_random_position():
	return Vector2(
		position.x + WINDOW_WIDTH,
		spawn_area.get_random_vertical_position())
