extends StaticBody2D

signal checkpoint_reached

onready var notifier = $VisibilityNotifier2D
onready var checkpoint = $Checkpoint
onready var anim_player = $AnimationPlayer


func _ready():
	var p = global_position
	set_as_toplevel(true)
	global_position = p

	notifier.connect("screen_exited", self, "queue_free")
	checkpoint.connect("body_entered", self, "_on_Checkpoint_body_entered")


func _on_Checkpoint_body_entered(body):
	emit_signal("checkpoint_reached")


func fade_out():
	anim_player.play("fade_out")
