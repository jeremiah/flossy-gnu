tool
extends Node2D
"""
Repeats a sprite hozitontally infinitely by spawning enough sprites to always
fill the view
"""

onready var spawn_position = $SpawnPosition

export(PackedScene) var sprite_scene

var WINDOW_WIDTH = ProjectSettings.get('display/window/size/width')
var sprite_width = 0.0


func _ready():
	assert sprite_scene
	if Engine.editor_hint:
		spawn_position.add_child(sprite_scene.instance())
	else:
		var sprite = sprite_scene.instance()
		sprite_width = sprite.texture.get_width() * sprite.scale.x
		var amount_to_fill_screen = ceil(WINDOW_WIDTH / sprite_width)
		for i in range(amount_to_fill_screen + 2):
			spawn_sprite()


func spawn_sprite():
	"""Offsets the spawner node and spawns a new sprite"""
	var sprite = sprite_scene.instance()
	sprite.connect("tree_exited", self, "spawn_sprite")
	sprite.position = spawn_position.position
	call_deferred("add_child", sprite) # to avoid error on reload
	spawn_position.position.x += sprite_width
